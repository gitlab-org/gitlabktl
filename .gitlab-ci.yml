image: golang:1.14

include:
  template: Serverless.gitlab-ci.yml

stages:
  - test    # unit tests and static analysis
  - build   # build a binary
  - release # release an image
  - prepare # prepare images
  - deploy  # deploy images
  - verify  # verify end-to-end

.go-cache:
  variables:
    GOPATH: $CI_PROJECT_DIR/.go
  before_script:
    - mkdir -p .go
  cache:
    paths:
      - .go/pkg/mod/

unit:tests:
  stage: test
  extends: .go-cache
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - go test ./... -cover -covermode=count -coverprofile=coverage.out -v | tee tests-output.txt
    - go tool cover -o coverage.html -html=coverage.out
    - go tool cover -o coverage.txt -func=coverage.out
    - grep "total" coverage.txt
    - go get -u github.com/jstemmer/go-junit-report
    - $CI_PROJECT_DIR/.go/bin/go-junit-report < tests-output.txt > junit-report.xml
  coverage: /^total:\s+\(statements\)\s+\d+.\d+\%/
  artifacts:
    reports:
      junit: junit-report.xml
    paths:
      - coverage.html
    expire_in: 7d

code:quality:
  stage: test
  image: docker:git
  services:
    - docker:dind
  cache: {}
  dependencies: []
  tags:
    - docker
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    CODECLIMATE_FORMAT: json
  script:
    - apk add -U --no-cache make > /dev/null
    - make -s lint -e CODECLIMATE_FORMAT=json | tee gl-code-quality-report.json
    - |
      if [ "$(cat gl-code-quality-report.json)" != "[]" ] ; then
        apk add -U --no-cache jq > /dev/null
        jq -C . gl-code-quality-report.json
        exit 1
      fi
  artifacts:
    paths:
      - gl-code-quality-report.json
    expire_in: 7d

check:go-mod-tidiness:
  stage: test
  script:
    - go mod tidy
    - |
      STATUS=$( git status --porcelain go.mod go.sum )
      if [ ! -z "$STATUS" ]; then
        echo "Running go mod tidy modified go.mod and/or go.sum"
        exit 1
      fi

.on-end-to-end:
  only:
    variables:
      - $CI_COMMIT_TAG                    # on tags
      - $CI_COMMIT_REF_NAME == "master"   # on master
      - $CI_PIPELINE_SOURCE == "schedule" # on schedules
      - $CI_COMMIT_MESSAGE  =~ /gitlabktl end-to-end/

.image-on-commit:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

build:binary:
  stage: build
  extends:
    - .go-cache
    - .on-end-to-end
  script: make build
  artifacts:
    paths:
      - gitlabktl
    expire_in: 21d

release:image:
  stage: release
  image: registry.gitlab.com/gitlab-org/gitlabktl:latest
  extends: .on-end-to-end
  variables:
    GITLAB_APP_TAG: $CI_COMMIT_SHORT_SHA
  script:
    - md5sum gitlabktl
    - |
      if [ -z "$CI_COMMIT_TAG" ]; then
        echo "Building $GITLAB_APP_TAG test image ..."
        /usr/bin/gitlabktl app build
      else
        IMAGE_TAG=$(echo $CI_COMMIT_TAG | sed 's/^v//')
        echo "Building $GITLAB_APP_TAG, $IMAGE_TAG images and 'latest' release ..."
        /usr/bin/gitlabktl app build --alias $IMAGE_TAG --alias latest
      fi
  dependencies:
    - build:binary

build:functions:
  extends:
    - .serverless:build:functions
    - .on-end-to-end
    - .image-on-commit
  stage: prepare
  before_script:
    - cd specs/fixtures
    - echo $CI_COMMIT_SHA > echo/uuid.txt
    - echo $CI_COMMIT_SHA > openfaas-classic-ruby/function/uuid.txt
  dependencies: []

build:app:
  extends:
    - .serverless:build:image
    - .on-end-to-end
    - .image-on-commit
  stage: prepare
  before_script:
    - cd specs/fixtures/app
    - echo $CI_COMMIT_SHA > uuid.txt
  variables:
    GITLAB_APP_IMAGE: $CI_REGISTRY_IMAGE/gitlabktl-tests-app-hello
  dependencies: []

deploy:functions:
  extends:
    - .serverless:deploy:functions
    - .on-end-to-end
    - .image-on-commit
  environment: testing
  stage: deploy
  retry: 1
  before_script:
    - cd specs/fixtures/
  dependencies: []

deploy:app:
  extends:
    - .serverless:deploy:image
    - .on-end-to-end
    - .image-on-commit
  environment: testing
  stage: deploy
  retry: 1
  variables:
    GITLAB_APP_NAME: gitlabktl-tests-app-hello
    GITLAB_APP_IMAGE: $CI_REGISTRY_IMAGE/gitlabktl-tests-app-hello
  dependencies: []

verify:end-to-end:
  extends: .on-end-to-end
  image: ruby:2.7
  environment: testing
  stage: verify
  retry:
    max: 2
    when: script_failure
  before_script:
    - cd specs/
    - bundle install
  dependencies: []
  script:
    - bundle exec rspec
