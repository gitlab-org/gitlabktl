FROM gcr.io/kaniko-project/executor:debug-v1.3.0
COPY gitlabktl /usr/bin/gitlabktl

ENV KUBECTL_VERSION=v1.16.2
ENV PATH $PATH:/usr/bin

SHELL ["/busybox/sh", "-c"]
RUN mkdir -p /usr/bin/ \
 && wget -O /usr/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl \
 && chmod +x /usr/bin/kubectl

WORKDIR /root
ENTRYPOINT []
